﻿using System;
using System.Collections;
using _Project.Scripts.Player;
using _Project.Scripts.Player.GUI;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace _Project.Tests.EditMode
{
    public class ExperienceBarTests
    {
        private const int ZERO = 0;
        private const int POSITIVE1 = 1;
        private const int POSITIVE10 = 10;
        private const int POSITIVE20 = 20;
        private const int POSITIVE100 = 100;

        private readonly Image _image = new GameObject().AddComponent<Image>();
        private Experience _exp = new Experience(ZERO, POSITIVE100);

        public class TheImageAttribute : ExperienceBarTests
        {
            [Test]
            public void New_ImageNull_ThrowException()
            {
                Assert.Throws<ArgumentException>(() =>
                {
                    ExperienceBar experienceBar = new ExperienceBar(null, _exp);
                });
            }

            [Test]
            public void New_Image_NoException()
            {
                Assert.DoesNotThrow(() =>
                {
                    ExperienceBar experienceBar = new ExperienceBar(_image, _exp);
                });
            }

            [Test]
            public void New_ImageFillAmountIsExperiencePercentage_ReturnTrue()
            {
                Image image = new GameObject().AddComponent<Image>();
                image.fillAmount = 1;
                
                ExperienceBar bar = new ExperienceBar(image, _exp);
                
                Assert.AreEqual(ZERO, image.fillAmount);
            }
        }

        public class TheExperienceProperty : ExperienceBarTests
        {
            // [Test]
            // public void New_Experience_ReturnExperience()
            // {
            //     ExpereienceBar expereienceBar = new ExpereienceBar(_image, _exp);
            //
            //     Assert.AreEqual(_exp, expereienceBar.Experience);
            // }

            [Test]
            public void New_ExperienceIsNull_ThrowException()
            {
                Assert.Throws<ArgumentException>(() =>
                {
                    ExperienceBar experienceBar = new ExperienceBar(_image, null);
                });
            }
        }

        public class TheIncreaseMethod : ExperienceBarTests
        {
            [SetUp]
            public void BeforeEachTest()
            {
                _exp = new Experience(ZERO, POSITIVE100);
            }
            
            [UnityTest]
            public IEnumerator Increase_10In1Second_FillAmount10Percent()
            {
                ExperienceBar experienceBar = new ExperienceBar(_image, _exp);
                _exp.Increase(POSITIVE10);

                yield return experienceBar.Increase(POSITIVE10, 1);
                
                Assert.AreEqual(0.1f, _image.fillAmount, 0.01f);
            }
            [UnityTest]
            public IEnumerator Increase_10In2Seconds_FillAmount20Percent()
            {
                ExperienceBar experienceBar = new ExperienceBar(_image, _exp);
                _exp.Increase(POSITIVE20);

                yield return experienceBar.Increase(POSITIVE10, 2);
                
                Assert.AreEqual(0.2f, _image.fillAmount, 0.01f);
            }
            
            [UnityTest]
            public IEnumerator Increase_10In1Second2Times_FillAmount20Percent()
            {
                ExperienceBar experienceBar = new ExperienceBar(_image, _exp);
                
                _exp.Increase(POSITIVE10);
                yield return experienceBar.Increase(POSITIVE10, 1);
                
                _exp.Increase(POSITIVE10);
                yield return experienceBar.Increase(POSITIVE10, 1);
                
                Assert.AreEqual(0.2f, _image.fillAmount, 0.01f);
            }
            
            [UnityTest]
            public IEnumerator Increase_100Percent_ReturnFillAmount1()
            {
                const float delta = 0.01f;
                ExperienceBar experienceBar = new ExperienceBar(_image, _exp);
                _exp.Increase(POSITIVE100);

                yield return experienceBar.Increase(POSITIVE100, 1);
                
                Assert.AreEqual(POSITIVE1, _image.fillAmount, delta);
            }
        }
    }
}