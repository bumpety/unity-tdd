﻿using System;
using _Project.Scripts.Helper;
using NUnit.Framework;

namespace _Project.Tests.EditMode
{
    public class CalculatorTests
    {
        private const int ZERO = 0;
        private const int ONE = 1;
        private const int TEN = 10;
        private const int FIFTY = 50;
        private const int HUNDRED = 100;
        private const int THOUSEND = 1000;

        public class ThePercentageMethod
        {
            [Test]
            public void Percentage_ValueEqualsMin_ReturnMin()
            {
                float result = Calculator.Percentage(ZERO, ZERO, HUNDRED);

                Assert.AreEqual(ZERO, result);
            }

            [Test]
            public void Percentage_ValueEqualsMax_ReturnMax()
            {
                float result = Calculator.Percentage(HUNDRED, ZERO, HUNDRED);
                
                Assert.AreEqual(HUNDRED, result);
            }

            [Test]
            public void Percentage_ValueTen_IsTenPercentOfHundred()
            {
                float result = Calculator.Percentage(TEN, ZERO, HUNDRED);

                Assert.AreEqual(TEN, result);
            }

            [Test]
            public void Percentage_ValueTen_IsOnePercentOfThousand()
            {
                float result = Calculator.Percentage(TEN, ZERO, THOUSEND);

                Assert.AreEqual(ONE, result);
            }

            [Test]
            public void Percentage_ValueLowerThanMin_ThrowException()
            {
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                {
                    Calculator.Percentage(ZERO, TEN, HUNDRED);
                });
            }

            [Test]
            public void Percentage_ValueHigherThanMax_ThrowException()
            {
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                {
                    Calculator.Percentage(HUNDRED, ZERO, TEN);
                });
            }

            [Test]
            public void Percentage_Value400_50PercentOf500WithMin300()
            {
                const int value = 400;
                const int min = 300;
                const int max = 500;
                
                float result = Calculator.Percentage(value, min, max);
                
                Assert.AreEqual(FIFTY, result);
            }
        }
    }
}