﻿using System;
using _Project.Scripts.Player;
using NUnit.Framework;

namespace _Project.Tests.EditMode
{
    public class ProgressBarTests
    {
        private const int NEGATIVE_100 = -100;
        private const int NEGATIVE_10 = -10;
        private const int NEGATIVE_1 = -1;
        private const int ZERO = 0;
        private const int POSITIVE_1 = 1;
        private const int POSITIVE_10 = 10;
        private const int POSITIVE_100 = 100;

        public class TheHighestProperty
        {
            [Test]
            public void New_HighestIsPositive10_Return10()
            {
                ProgressBar progressBar = new ProgressBar(NEGATIVE_10, POSITIVE_10);

                Assert.AreEqual(POSITIVE_10, progressBar.Highest);
            }

            [Test]
            public void New_HighestIsNegative10_ReturnNegative10()
            {
                ProgressBar progressBar = new ProgressBar(NEGATIVE_100, NEGATIVE_10);

                Assert.AreEqual(NEGATIVE_10, progressBar.Highest);
            }

            [Test]
            public void New_HighestIsLowerThanLowest_ThrowException()
            {
                Assert.Throws<ArgumentException>(() =>
                {
                    ProgressBar bar = new ProgressBar(ZERO, NEGATIVE_1);
                });
            }
        }

        public class TheLowestProperty
        {
            [Test]
            public void New_LowestIsNegative10_ReturnNegative10()
            {
                ProgressBar progressBar = new ProgressBar(NEGATIVE_10, POSITIVE_10);

                Assert.AreEqual(NEGATIVE_10, progressBar.Lowest);
            }

            [Test]
            public void New_LowestIsNegative100_ReturnNegative100()
            {
                ProgressBar progressBar = new ProgressBar(NEGATIVE_100, NEGATIVE_10);

                Assert.AreEqual(NEGATIVE_100, progressBar.Lowest);
            }
        }

        public class TheCurrentProperty
        {
            [Test]
            public void New_DefaultIsLowest_ReturnLowestValue()
            {
                ProgressBar progressBar = new ProgressBar(POSITIVE_1, POSITIVE_10);

                Assert.AreEqual(POSITIVE_1, progressBar.Current);
            }

            [Test]
            public void New_CurrentIsNegativeOne_ReturnNegativeOne()
            {
                ProgressBar progressBar = new ProgressBar(NEGATIVE_10, POSITIVE_10, NEGATIVE_1);

                Assert.AreEqual(NEGATIVE_1, progressBar.Current);
            }

            [Test]
            public void New_CurrentIsHigherThanHighest_ThrowException()
            {
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                {
                    ProgressBar progressBar = new ProgressBar(NEGATIVE_10, ZERO, POSITIVE_1);
                });
            }

            [Test]
            public void New_CurrentIsLowerThanLowest_ThrowException()
            {
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                {
                    ProgressBar progressBar = new ProgressBar(ZERO, POSITIVE_10, NEGATIVE_1);
                });
            }

            [Test]
            public void New_CurrentIsSameAsLowest_ReturnLowest()
            {
                ProgressBar progressBar = new ProgressBar(NEGATIVE_10, POSITIVE_10, NEGATIVE_10);

                Assert.AreEqual(NEGATIVE_10, progressBar.Current);
            }

            [Test]
            public void New_CurrentIsSameAsHighest_ReturnHighest()
            {
                ProgressBar progressBar = new ProgressBar(NEGATIVE_10, POSITIVE_10, POSITIVE_10);

                Assert.AreEqual(POSITIVE_10, progressBar.Current);
            }

            [Test]
            public void New_CurrentIsBetweenLowestAndHighest_ReturnCurrent()
            {
                ProgressBar progressBar = new ProgressBar(NEGATIVE_10, POSITIVE_10, ZERO);

                Assert.AreEqual(ZERO, progressBar.Current);
            }
        }

        public class TheIncreaseMethod
        {
            private ProgressBar _progressBar;

            [SetUp]
            public void BeforeEachTest()
            {
                _progressBar = new ProgressBar(NEGATIVE_10, POSITIVE_10);
            }

            [Test]
            public void Increase_Positive10_IncreaseCurrentBy10()
            {
                _progressBar.Increase(POSITIVE_10);

                Assert.AreEqual(ZERO, _progressBar.Current);
            }

            [Test]
            public void Increase_OverTheHighestValue_ReturnHighestValue()
            {
                _progressBar.Increase(POSITIVE_100);

                Assert.AreEqual(POSITIVE_10, _progressBar.Current);
            }

            [Test]
            public void Increase_NegativeValue_ThrowException()
            {
                Assert.Throws<ArgumentOutOfRangeException>(() => { _progressBar.Increase(NEGATIVE_10); });
            }
        }

        public class TheDecreaseMethod
        {
            private ProgressBar _progressBar;

            [SetUp]
            public void BeforeEachTest()
            {
                _progressBar = new ProgressBar(NEGATIVE_10, POSITIVE_10);
            }

            [Test]
            public void Decrease_Positive10_DecreaseCurrentBy10()
            {
                _progressBar.Decrease(POSITIVE_10);

                Assert.AreEqual(NEGATIVE_10, _progressBar.Current);
            }

            [Test]
            public void Decrease_UnderTheLowestValue_ReturnLowestValue()
            {
                _progressBar.Decrease(POSITIVE_100);

                Assert.AreEqual(NEGATIVE_10, _progressBar.Current);
            }

            [Test]
            public void Decrease_NegativeValue_ThrowException()
            {
                Assert.Throws<ArgumentOutOfRangeException>(() => { _progressBar.Decrease(NEGATIVE_10); });
            }
        }
    }
}