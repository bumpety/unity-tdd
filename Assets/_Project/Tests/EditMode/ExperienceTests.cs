using System;
using _Project.Scripts.Player;
using NUnit.Framework;

namespace _Project.Tests.EditMode
{
    public class ExperienceTests
    {
        private const int NEGATIVE_1 = -1;
        private const int ZERO = 0;
        private const int POSITIVE_1 = 1;
        private const int POSITIVE_10 = 10;
        private const int POSITIVE_100 = 100;
        private const int POSITIVE_200 = 200;

        private const int TEN_EXP = 10;

        public class TheMinimumProperty
        {
            [Test]
            public void New_NegativeMinimum_ThrowsException()
            {
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                {
                    Experience experience = new Experience(NEGATIVE_1, POSITIVE_1);
                });
            }

            [Test]
            public void New_MinimumLargerThanMaximum_ThrowException()
            {
                Assert.Throws<ArgumentException>(() =>
                {
                    Experience experience = new Experience(POSITIVE_10, POSITIVE_1);
                });
            }

            [Test]
            public void New_Minimum10_ReturnMinimum10()
            {
                Experience experience = new Experience(POSITIVE_10, POSITIVE_100);

                Assert.AreEqual(POSITIVE_10, experience.Minimum);
            }
        }

        public class TheMaximumProperty
        {
            [Test]
            public void New_NegativeMaximum_ThrowsExeption()
            {
                Assert.Throws<ArgumentException>(() =>
                {
                    Experience experience = new Experience(ZERO, NEGATIVE_1);
                });
            }

            [Test]
            public void New_ZeroMaximum_ThrowsException()
            {
                Assert.Throws<ArgumentException>(() =>
                {
                    Experience experience = new Experience(ZERO, ZERO);
                });
            }

            [Test]
            public void New_MaximumOfOne_ReturnsMaximumOfOne()
            {
                Experience bar = new Experience(ZERO, POSITIVE_1);
                Assert.AreEqual(POSITIVE_1, bar.Maximum);
            }
        }

        public class TheCurrentProperty
        {
            [Test]
            public void New_Default_ReturnZeroExperience()
            {
                Experience exp = new Experience(ZERO, POSITIVE_100);
                Assert.AreEqual(0, exp.Current);
            }

            [Test]
            public void New_10Experience_Return10Experience()
            {
                Experience bar = new Experience(ZERO, POSITIVE_100, TEN_EXP);
                Assert.AreEqual(TEN_EXP, bar.Current);
            }
        }

        public class TheIncreaseMethod
        {
            private Experience _bar;

            [SetUp]
            public void BeforeEachTest()
            {
                _bar = new Experience(ZERO, POSITIVE_100);
            }

            [Test]
            public void Increase_NegativeOne_ThrowException()
            {
                Assert.Throws<ArgumentOutOfRangeException>(() =>
                {
                    _bar.Increase(NEGATIVE_1);
                });
            }

            [Test]
            public void Increase_PositiveOne_CurrentIsOne()
            {
                _bar.Increase(POSITIVE_1);

                Assert.AreEqual(POSITIVE_1, _bar.Current);
            }

            [Test]
            public void Increase_Positive200_Return100ByMaximumOf100()
            {
                _bar.Increase(POSITIVE_200);

                Assert.AreEqual(POSITIVE_100, _bar.Current);
            }
        }

        public class TheIncreasedEvent
        {
            [Test]
            public void Raises_Event_On_Increased()
            {
                int exp = ZERO;
                Experience experience = new Experience(ZERO, POSITIVE_100);

                experience.Increased += (sender, args) =>
                {
                    exp = args.Amount;
                };
                
                experience.Increase(POSITIVE_10);
                
                Assert.AreEqual(POSITIVE_10, exp);
            }
        }
    }
}