﻿using System;

namespace _Project.Scripts.Player
{
    /// <summary>
    /// Class for handling experience of a player.
    /// </summary>
    public class Experience
    {
        public int Current { get; private set; }
        public int Minimum { get; }
        public int Maximum { get; }

        public event EventHandler<IncreasedEventArgs> Increased;

        public Experience(int minimum, int maximum)
        {
            if (minimum < 0)
                throw new ArgumentOutOfRangeException(
                    $"Minimum can't be a negetaive value of {minimum}.");

            if (minimum >= maximum)
                throw new ArgumentException(
                    $"Minimum of {minimum} has to be lower than maximum of {maximum}");

            Current = minimum;
            Minimum = minimum;
            Maximum = maximum;
        }

        public Experience(int minimum, int maximum, int current) : this(minimum, maximum)
        {
            if (current > maximum)
                throw new ArgumentException(
                    $"Current value of {current} has to be larger than maximum of {maximum}");

            Current = current;
        }

        /// <summary>
        /// Increase current exp by the amount and ignore the overflow.
        /// </summary>
        /// <param name="amount"></param>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public void Increase(int amount)
        {
            if (amount < 0)
                throw new ArgumentOutOfRangeException(
                    $"Experience can't get increased with negative values.");

            int realAmount = Math.Min(Current + amount, Maximum);
            Current = realAmount;

            Increased?.Invoke(this, new IncreasedEventArgs(realAmount));
        }
    }
}