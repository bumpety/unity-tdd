﻿using System;

namespace _Project.Scripts.Player
{
    /// <summary>
    /// A Progressbar for a range between a negative value and any higher value of the same type.
    /// </summary>
    public class ProgressBar 
    {
        public int Lowest { get; }
        public int Highest { get; }
        public int Current { get; private set; }

        public ProgressBar(int lowest, int highest)
        {
            if (highest <= lowest)
                throw new ArgumentException("Highest value has to be greater than lowest value.");

            Lowest = lowest;
            Highest = highest;
            Current = lowest;
        }

        public ProgressBar(int lowest, int highest, int current) : this(lowest, highest)
        {
            if (current > highest)
                throw new ArgumentOutOfRangeException(nameof(current));

            if (current < lowest)
                throw new ArgumentOutOfRangeException(nameof(current));

            Current = current;
        }
        
        /// <summary>
        /// Increase current value in the direction of the Highest property.
        /// </summary>
        /// <param name="value">Value to be increased by.</param>
        /// <exception cref="ArgumentOutOfRangeException">Value is below zero.</exception>
        public void Increase(int value)
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException(nameof(value));

            Current = Math.Min(Current + value, Highest);
        }

        /// <summary>
        /// Decreases current value in the direction of the Lowest property.
        /// </summary>
        /// <param name="value">Value to be decreased by.</param>
        /// <exception cref="ArgumentOutOfRangeException">Value is below zero.</exception>
        public void Decrease(int value)
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException(nameof(value));

            Current = Math.Max(Current - value, Lowest);
        }
    }
}