﻿using System;

namespace _Project.Scripts.Player
{
    /// <summary>
    /// Event for an increased value.
    /// </summary>
    public class IncreasedEventArgs : EventArgs
    {
        public int Amount { get; }
            
        public IncreasedEventArgs(int amount)
        {
            Amount = amount;
        }
    }
}