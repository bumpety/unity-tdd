using UnityEngine;

namespace _Project.Scripts.Player
{
    public class Player : MonoBehaviour
    {
        [SerializeField]
        private int minimum = 300;

        [SerializeField]
        private int maximum = 900;

        [SerializeField]
        private int current = 400;

        public Experience Experience { get; private set; }
    
        private void Awake()
        {
            Experience = new Experience(minimum, maximum, current);
        }

        public void TakeExp(int amount)
        {
            Experience.Increase(amount);
        }
    }
}