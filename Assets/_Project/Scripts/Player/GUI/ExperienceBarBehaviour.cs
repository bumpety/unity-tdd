using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Player.GUI
{
    public class ExperienceBarBehaviour : MonoBehaviour
    {
        [SerializeField]
        private Image fillImage;

        [SerializeField]
        private float animationTime = 3f;

        private ExperienceBar _experienceBar;
        private Coroutine _increaseOverTimeRoutine;
        private Player _player;

        private void Awake()
        {
            _player = FindObjectOfType<Player>();
        }

        private void Start()
        {
            _experienceBar = new ExperienceBar(fillImage, _player.Experience);
        }

        private void OnEnable()
        {
            _player.Experience.Increased += Increase;
        }

        private void OnDisable()
        {
            _player.Experience.Increased -= Increase;
        }

        private void Increase(object sender, IncreasedEventArgs increasedEventArgs)
        {
            if (_increaseOverTimeRoutine != null)
            {
                StopCoroutine(_increaseOverTimeRoutine);
            }

            _increaseOverTimeRoutine = 
                StartCoroutine(_experienceBar.Increase(increasedEventArgs.Amount, animationTime));
        }
    }
}