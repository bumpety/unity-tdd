﻿using System;
using System.Collections;
using _Project.Scripts.Helper;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.Player.GUI
{
    public class ExperienceBar 
    {
        private readonly Image _bar;
        private readonly Experience _experience;
        private float _timeElapsed;

        public ExperienceBar(Image bar, Experience experience)
        {
            if (!bar)
                throw new ArgumentException("Sprite of the bar cannot be null.");

            if (experience == null)
                throw new ArgumentException("Experience cannot be null.");

            _bar = bar;
            _experience = experience;
            
            SetFillAmount();
        }

        public IEnumerator Increase(int amount, float animationTime = 0) {
        
            _timeElapsed = 0;
            float a = _bar.fillAmount;
            float b = Calculator.Percentage(_experience.Current, _experience.Minimum, _experience.Maximum) / 100;

            while (_timeElapsed < animationTime)
            {
                float value = Mathf.Lerp(a, b, _timeElapsed / animationTime);
                _bar.fillAmount = value;

                _timeElapsed += Time.deltaTime;
                yield return null;
            }
            
            yield return null;

            _bar.fillAmount = b;
        }

        private void SetFillAmount()
        {
            _bar.fillAmount =
                Calculator.Percentage(_experience.Current, _experience.Minimum, _experience.Maximum) / 100;
        }
    }
}