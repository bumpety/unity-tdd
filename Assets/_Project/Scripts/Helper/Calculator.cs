﻿using System;

namespace _Project.Scripts.Helper
{
    /// <summary>
    /// Basic calculator helper class.
    /// </summary>
    public static class Calculator
    {
        /// <summary>
        /// Calculates the percentage of a value between a min and a max value.
        /// </summary>
        /// <param name="value">The value between min and max.</param>
        /// <param name="min">The lower end of the calculation. (0%)</param>
        /// <param name="max">The upper end of the calculation. (100%)</param>
        /// <returns>Percentage of the value between the others.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Value is lower than min or higher than max.</exception>
        public static float Percentage(int value, int min, int max)
        {
            if (value < min)
                throw new ArgumentOutOfRangeException(nameof(value));

            if (value > max)
                throw new ArgumentOutOfRangeException(nameof(value));

            value -= min;
            max -= min;

            return (float) value / max * 100f;
        }
    }
}