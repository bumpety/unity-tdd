# Unity Test Driven Development / Test Guided Development

This is a project with some components that were developed with the tdd workflow. However, during the development process I thought that TDD is sometimes an overkill. So I decided to test sometimes only the things that would be critical in an development process (like null checks). Also there are MonoBehaviours that could be tested in an PlayMode test but aren't tested because the logic of these components are very simple and easy to check. But a PlayMode test would take much more time to check that simple piece of code. There are tests for the ExperienceBar Increase - Coroutine that is tested as a UnityTest. This simple test checks if the coroutine does change the fillAmount after n seconds correctly. This test shows how long a UnityTest take place.

The examples contains a player with an experience bar. The image of the bar representing the experience. After the experience of the player is changed, the bar receives an event and changes the fillAmount field of the image in a coroutine.

There is also a small helper class for percentage calculation that is also fully tested.

## Edit Mode Tests

To run the tests, open the Test Runner under 
__Window > General > Test Runner__
